var SAAgent = null;
var SASocket = null;

var CHANNELID = 1024;
var ConsumerAppName = "Navindo";

// Element to activate/deactivate/remove on callback (used when deleting lists or removing, activating or deactivating products)
var productOrListChangeElement;
var productOrListChangeElementInnerHTML;

function onerror(err) {
	console.log(err);
	console.log("ERROR: onerror | " + err.name + " | " + err.message);
}

var Commands = {
		removeProduct : "remove_product",
		activateProduct : "activate_product",
		deactivateProduct : "deactivate_product",
		getLists : "get_lists",
		getListData : "get_list_data",
		listChanged : "list_changed",
		listDeleted : "list_deleted",
		deleteList : "delete_list",
		cartIdChanged : "cart_id_changed",
		refresh : "refresh",
		productDistance : "product_distance",
		activateList : "activate_list",
		loginCartsession: "login_cartsession",
		logoutCartsession: "logout_cartsession"
};

function getListsHandler(data) {
	var lists = JSON.parse(data["lists"]);
	// Empty lists list 
	document.getElementById("lists").innerHTML = "";
	
	if (lists.length === 0) {
		// Alert if no lists are received
		alert("You have no lists!");	
	}	
	
	// Get lists page for page-hide handling and lists list for TAU swipe list
	var page = document.getElementById("lists-page"), listsList = document.getElementById("lists");
	
	for (var key in lists) {
		if (lists.hasOwnProperty(key)) {
			number_of_products = lists[key]["products"].length;
			if (number_of_products > 0) {
				listsList.innerHTML += "<li data-id=" + lists[key]["id"] + " onclick=\"onListChosen(" + 
										lists[key]["id"] + ");\"><a>" + lists[key]["name"] + " | " + 
										lists[key]["products"].length + "</a></li>";
			}
			else {
				listsList.innerHTML += "<li data-id=" + lists[key]["id"] + " onclick=\"onListChosen(" + 
									lists[key]["id"] + ");\">" + lists[key]["name"] + " | " + 
									lists[key]["products"].length + "</li>";
			}
		}
	}
	
	// Initialize swipe list widget
	var swipeList = new tau.widget.SwipeList(listsList, {
		swipeTarget: "li",
		swipeElement: ".ui-swipelist"
	});
	
	// Define actions for any swipe
	function listSwipeCallback(event) {
		var target = event.target;
		var list_id = target.dataset["id"];
		productOrListChangeElement = target;
		
		// Open confirmation popup
		tau.openPopup("#deleteListPopup");

		// Add handler for popup closing on cancel button press
		document.getElementById('deleteListPopupFalse').addEventListener('click', function(ev) {
			tau.closePopup();
		});
		
		// Add handler for list delete actions on OK button press
		document.getElementById('deleteListPopupTrue').addEventListener('click', function(ev) {
			var list_id = productOrListChangeElement.dataset["id"];
			sendCommand(Commands.deleteList, list_id);
			productOrListChangeElement.innerHTML = "<div class=\"ui-processing\"></div>";		
			tau.closePopup();					
		});
	}	
	
	// Register any (left and right) swipe event listener	
	listsList.addEventListener("swipelist.left", listSwipeCallback);
	listsList.addEventListener("swipelist.right", listSwipeCallback);
	
	page.addEventListener("pagehide", function() {
		/* Release object */
		swipeList.destroy();
		
		// Remove event listeners
		listsList.removeEventListener("swipelist.left", listSwipeCallback);
		listsList.removeEventListener("swipelist.right", listSwipeCallback);
	});
	
}

function getListDataHandler(data) {

	var listData = JSON.parse(data["list_data"]);
	// 2nd element is a list object containing id, list of products and other data
	var list_id = listData["id"];			
	var products = listData["products"];
	
	// Get products page for page-hide handling and products list for TAU swipe list
	var page = document.getElementById("products-page");
	
	var productsListParent = document.getElementById("products").parentNode;
	var productsList = document.getElementById("products");
	productsList.innerHTML = "";
	
	var newProductsList = productsList.cloneNode(true);
	productsListParent.removeChild(productsList);
	productsListParent.insertBefore(newProductsList, productsListParent.childNodes[0]);
	productsList = newProductsList;
	
	if (products.length == 0) {
		// Alert if no products in list, this should not be reachable 
		// because the same check is made when selecting a list to display products
		alert("This lists has no products!");
		window.history.back();
	}
	
	for (var key in products) {
		if (products.hasOwnProperty(key)) {
			// Append list item for every product from JSON, add relevant data (id, active)
			productsList.innerHTML += "<li " + (products[key]["active"]?"":"class=deactivated ") + 
										"data-id=" + products[key]["productid"] + " data-active=" + products[key]["active"] + 
										">" + products[key]["name"] + "</li>";
		}
	}		
   
	// Initialize swipe list widget
	var swipeList = new tau.widget.SwipeList(productsList, {
		swipeTarget: "li",
		swipeElement: ".ui-swipelist"
	});
	
	// Define actions for left swipe
	function productLeftSwipeCallback(event) {
		var target = event.target;
		var product_id = target.dataset["id"];
		var active = target.dataset["active"];
		var innerHTML = target.innerHTML;
		var list_id = document.getElementById("products-page").dataset["list_id"];
		
		productOrListChangeElement = target;
		productOrListChangeElementInnerHTML = target.innerHTML;
		
		target.innerHTML = "<div class=\"ui-processing\"></div>";				
		
		if (active === "true") {
			sendCommand(Commands.deactivateProduct, list_id, product_id);
			//deactivateProduct(list_id, product_id);
		}else {
			sendCommand(Commands.activateProduct, list_id, product_id);
			//activateProduct(list_id, product_id);
		}
	}
	
	// Define actions for right swipe
	function productRightSwipeCallback(event) {
		var target = event.target;
		var list_id = document.getElementById("products-page").dataset["list_id"];
		var product_id = target.dataset["id"];
		
		target.innerHTML = "<div class=\"ui-processing\"></div>";
		
		productOrListChangeElement = target;
		//removeProduct(list_id, product_id)
		sendCommand(Commands.removeProduct, list_id, product_id);
	}				
	
	// Register left (delete) swipe event listener			
	productsList.addEventListener("swipelist.left", productLeftSwipeCallback);
	
	// Register right (activate/deactivate) swipe event listener
	productsList.addEventListener("swipelist.right", productRightSwipeCallback);	
	
	page.addEventListener("pagehide", function() {
		/* Release object */
		swipeList.destroy();
		
		// Remove event listeners
		productsList.removeEventListener("swipelist.left", productLeftSwipeCallback);
		productsList.removeEventListener("swipelist.right", productRightSwipeCallback);
	});
}

function activateProductHandler() {
	productOrListChangeElement.innerHTML = productOrListChangeElementInnerHTML;
	productOrListChangeElement.className = "";
	productOrListChangeElement.dataset["active"] = "true";
}

function deactivateProductHandler() {
	productOrListChangeElement.innerHTML = productOrListChangeElementInnerHTML;
	productOrListChangeElement.className = "deactivated";
	productOrListChangeElement.dataset["active"] = "false";
}

function listChangedHandler(data) {
	// Get updated list and redraw if id matches if not update lists
	var list_id = data["list_id"];
	console.log("listChangedHandler(data) - data: " + data);
	console.log("data[listid]:" + list_id);
	var current_list_id = document.getElementById("products-page").dataset["list_id"];
	var page = document.getElementsByClassName("ui-page-active")[0]
	  , pageid = page ? page.id : "";
	
	if (current_list_id == list_id && pageid === "products-page") {
		document.getElementById("products-page").dataset["list_id"] = list_id;
		document.getElementById("products").innerHTML = "<div class=\"ui-processing\"></div><div class=\"ui-processing-text\">Doing stuff...</div>";
		sendCommand(Commands.getListData, list_id);
	} else {
		getLists();
	}
}

function listDeletedHandler(data) {
	// Notify user and exit from list if id matches, update lists
	var list_id = data["list_id"];
	console.log("listDeletedHandler(data) - data: " + data);
	console.log("data[listid]:" + list_id);
	var current_list_id = document.getElementById("products-page").dataset["list_id"];
	var page = document.getElementsByClassName("ui-page-active")[0]
	  , pageid = page ? page.id : "";
	
	if (current_list_id == list_id && pageid === "products-page") {
		window.history.back();
		alert("Displayed list was deleted");
	}
	getLists();
}

function cartIdChangedHandler(data) {
	var cart_id = data["cart_id"];
	
	if (cart_id === "-1") {
		cart_id = "N/A";
		setCartRegistrationVisibility(true);
	}else {
		setCartRegistrationVisibility(false);
	}
	
	var elems = document.getElementsByClassName("cart-id");

	for (var i = 0;i < elems.length;i++) 
		elems[i].textContent = cart_id;
}

function productDistanceHandler(data) 
{
	var distance = data["distance"];
	
	document.getElementById("product-distance").textContent = "\u27B7 " + parseFloat(distance).toFixed(1);
}

/* Receiving logic */
function onReceive(channelId, data) {
	// 1st element of received JSON array is an object containing the name of the action
	var json = JSON.parse(data)
	  , receivedCommand = json[0]["for"]
	  , commandData = json[1];
	
	console.log("Received command: "+receivedCommand);

	if (receivedCommand === Commands.getLists) 
		getListsHandler(commandData);
	else if (receivedCommand === Commands.getListData) 
		getListDataHandler(commandData);
	else if (receivedCommand === Commands.removeProduct) 
		productOrListChangeElement.remove();
	else if (receivedCommand === Commands.activateProduct) 
		activateProductHandler();
	else if (receivedCommand === Commands.deactivateProduct) 
		deactivateProductHandler();
	else if (receivedCommand === Commands.deleteList) 
		productOrListChangeElement.remove();
	else if (receivedCommand === Commands.listChanged) 
		listChangedHandler(commandData);	
	else if (receivedCommand === Commands.listDeleted) 
		listDeletedHandler(commandData);
	else if (receivedCommand === Commands.cartIdChanged)
		cartIdChangedHandler(commandData);
	else if (receivedCommand === Commands.productDistance)
		productDistanceHandler(commandData);
}

function disconnect() 
{
	try 
	{
		if (SASocket !== null) 
		{
			SASocket.close();
			SASocket = null;
			console.log("Disconnected");
			// TODO: refactor connection status indicator
			var connStatusElement = document.getElementById("connection-status");
			connStatusElement.className = "connection-base disconnected";
		}
	} 
	catch(err) 
	{
		console.log("ERROR: Disconnect | " + err.name + " | " + err.message);
	}
}

function send(message) 
{
	console.log("Trying to send message:"+message);
	try 
	{
		SASocket.sendData(CHANNELID, message);
	} 
	catch(err) 
	{
		console.log("ERROR: send | " + err.name + " | " + err.message);
	}
}

var agentCallback = 
{
	onconnect : function(socket) 
	{
		SASocket = socket;

		console.log("Connected");
		
		// TODO: refactor connection status indicator
		var connStatusElement = document.getElementById("connection-status");
		connStatusElement.className = "connection-base connected";		

		SASocket.setDataReceiveListener(onReceive);	
		
		sendCommand(Commands.refresh);
		
		SASocket.setSocketStatusListener(function(reason)
		{
			console.log("ERROR: Service connection lost, Reason : [" + reason + "]");
			disconnect();
		});
	},
	onerror : onerror
};

var peerAgentFindCallback = 
{
	onpeeragentfound : function(peerAgent) 
	{
		try 
		{
			if (peerAgent.appName === ConsumerAppName) 
			{
				SAAgent.setServiceConnectionListener(agentCallback);
				SAAgent.requestServiceConnection(peerAgent);
			} 
			else 
			{
				console.log("Not expected application: " + peerAgent.appName);
			}
		} 
		catch(err) 
		{
			console.log("ERROR: peerAgentFindCallback | " + err.name + " | " + err.message);
		}
	},
	onerror : onerror
};

function onsuccess(agents) 
{
	try 
	{
		if (agents.length > 0) 
		{
			SAAgent = agents[0];
			
			SAAgent.setPeerAgentFindListener(peerAgentFindCallback);
			SAAgent.findPeerAgents();
		} 
		else 
		{
			console.log("Not found SAAgent!!");
		}
	} 
	catch(err) 
	{
		console.log("ERROR: onsuccess | " + err.name + " | " + err.message);
	}
}

function connect() 
{
	if (SASocket) 
	{
		console.log('Already connected!');
        return false;
    }
	
	try 
	{
		webapis.sa.requestSAAgent(onsuccess, onerror);
	} 
	catch(err) 
	{
		console.log("ERROR: Connect | " + err.name + " | " + err.message);
	}
}

function sendCommand(command) {
	var commandString = command;
	if (arguments.length > 1) {
		commandString += "::"
		for (var i = 1;i < arguments.length;i++) {
			commandString += arguments[i];
			if (i != arguments.length - 1) 
				commandString += ",";
		}
	}
	send(commandString);
}

function getLists() {
	sendCommand(Commands.getLists);
	document.getElementById("lists").innerHTML = "<div class=\"ui-processing\"></div><div class=\"ui-processing-text\">Doing stuff...</div>";
}

function onListChosen(list_id)
{
	sendCommand(Commands.getListData, list_id);
	tau.changePage("#products-page");
	document.getElementById("products-page").dataset["list_id"] = list_id;
	if (typeof(productDistancePollingInterval) === "undefined") {
		productDistancePollingInterval = window.setInterval(function distancePoll() {
			sendCommand(Commands.productDistance);
		}, 2000);	
	}	
}

function openCartRegistration() {
	tau.changePage("#cart-registration-page");
}

function btnInput(num) {
	var text_field = document.getElementById("text-field");
	if (text_field.textContent.length < 5) {
		text_field.textContent = text_field.textContent + num;
	}	
}

function btnBksp() {
	var text_field = document.getElementById("text-field");
	text_field.textContent = text_field.textContent.slice(0, -1);
}

function btnOk() {
	var text_field = document.getElementById("text-field");
	var cart_id = text_field.textContent;
	console.log("Cart id: " + cart_id);
	sendCommand(Commands.loginCartsession, cart_id);
}

function logoutCartsession() {
	sendCommand(Commands.logoutCartsession);
}

function setCartRegistrationVisibility(visible) {
	var btn_register_cart = document.getElementById("btn_register_cart");
	var btn_logout_cartsession = document.getElementById("btn_logout_cartsession");
	var page = document.getElementsByClassName('ui-page-active')[0]
	  , pageid = page ? page.id : "";
	
	if (visible) {
		btn_register_cart.style.display  = 'block';
		btn_logout_cartsession.style.display  = 'none';
	}else {
		btn_register_cart.style.display  = 'none';
		btn_logout_cartsession.style.display  = 'block';
	}
	
	if (pageid === "cart-registration-page") {
		window.history.back();
		alert("Cart successfully registered");
	}
}

window.onload = function () {
    // add eventListener for tizenhwkey
    document.addEventListener('tizenhwkey', function(e) {
        if(e.keyName == "back") {
        	var page = document.getElementsByClassName('ui-page-active')[0]
        	  , pageid = page ? page.id : "";
        	try {
        		if (pageid === "lists-page") {
        			tizen.application.getCurrentApplication().exit();
        		} 
        		else {
        			if (pageid === "products-page") {
        				if (typeof(!(productDistancePollingInterval === "undefined"))) {
            				clearInterval(productDistancePollingInterval);
            				delete productDistancePollingInterval;
            			}
            			getLists();
        			}       			
        			
        			window.history.back();
        		}        		
        	} catch (ignore) {
        		
        	}
        }	
    });

    connect();    
    
};